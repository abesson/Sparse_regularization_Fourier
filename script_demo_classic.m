% Script used to reproduce the results from the UFFC paper

clear all;
close all;
clc;
addpath(genpath('src'));

%-- Input parameters for the methods
number_plane_waves = 1;

%% Preprocess the data according to the number of plane waves

%-- Load the data 
load_file = 'data/rawdata_cyst_inclusion_5PW.mat';
load(load_file);
probe.pitch = probe.Pitch;
probe.c = probe.c0;

%-- Preprocess the data
[rf_data, probe] = select_data(data, steeringAngles, number_plane_waves, probe);

%-- Parameters used for the calculation of the contrast
xc = 0/1000;
zc = 40/1000;
r = 8/2/1000;
fact2 = 0.75;

%-- Generate image grid
x = probe.xm;
z = (0:size(rf_data, 1)-1)* probe.c0 / probe.fs / 2;

%% UFSB
%---------------------------------------------------------------------
%-- Reconstruct image using Ultrasound Fourier Slice Theory using compounding
disp('****** UFSB ******');
probe.settings = struct('flagfftRF',1, 'dxi', deg2rad(0.5), 'ximax', deg2rad(50));
[x_ufsb, z_ufsb, bmode_ufsb, migSIG_ufsb, ~] = ufsb(rf_data, probe);


%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_ufsb,'z',z_ufsb,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_bernard,~] = EvaluateContrasts(bmode_ufsb,parameters);
disp('****** UFSB done ******');

%% Garcia method
probe.t0 = 0;
disp('****** Garcia ******');
[x_garcia, z_garcia, bmode_garcia, migSIG_garcia, ~] = fkmig(rf_data,probe);

%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_garcia,'z',z_garcia,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_garcia,~] = EvaluateContrasts(bmode_garcia,parameters);
disp('****** Garcia done ******');

%% Lu method
disp('****** Lu ******');
[x_lu, z_lu, bmode_lu, migSIG_lu, ~] = lumig(rf_data,probe);

%---------------------------------------------------------------------
%-- Compute contrast
parameters = struct('x',x_lu,'z',z_lu,'xc',xc,'zc',zc,'r',r,'fact2',fact2);
[~,cr2_lu,info] = EvaluateContrasts(bmode_lu,parameters);
disp('****** Lu done ******');

%% Display
dBRange = 40;

%-- Range of the displayed area
minX = -12.5/1000;
maxX = 12.5/1000;
minZ = 5/1000;
maxZ = 55/1000;
area = struct('xmin',minX,'xmax',maxX,'zmin',minZ,'zmax',maxZ);

%-- UFSB
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_ufsb);
imagesc(x_ufsb*1000,z_ufsb*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['UFSB - ',num2str(number_plane_waves),' PWs | CR2 = ',num2str(cr2_bernard), ' dB']);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');

%-- Garcia
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_garcia);
imagesc(x_garcia*1000,z_garcia*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['Garcia - ',num2str(number_plane_waves),' PWs | CR2 = ',num2str(cr2_garcia), ' dB']);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');

%-- Lu
h = figure;
set(h,'Color',[1 1 1]);
bmode = ComputeLogCompressedBmode(bmode_lu);
imagesc(x_lu*1000,z_lu*1000,bmode); colormap(gray); caxis([-dBRange 0]);
axis image; axis([minX maxX minZ maxZ]*1000);
xlabel('Lateral position [mm]');
ylabel('Depth [mm]');
title(['Lu - ',num2str(number_plane_waves),' PWs | CR2 = ',num2str(cr2_lu), ' dB']);
h2 = colorbar;
set(h2,'YTick',[-40 -30 -20 -10 0]);
set(h2,'YTickLabel',{'-40 dB';'-30 dB';'-20 dB';'-10 dB';'0 dB'});
set(findall(h,'type','text'),'FontSize',11,'fontWeight','bold');
ha = get(h,'CurrentAxes');
set(ha,'FontSize',12,'fontWeight','bold');
        
