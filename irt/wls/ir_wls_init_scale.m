function x = ir_wls_init_scale(A, y)
x = A' * y;
tmp = A * x;
scale = sum(col(conj(tmp) .* y), 'double') / sum(col(abs(tmp).^2), 'double');
x = scale * x;
