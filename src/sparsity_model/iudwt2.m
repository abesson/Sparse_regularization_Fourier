function x = iudwt2(coef,W, filter)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

N1 = size(W,3);
Wf = zeros(size(W));
t = 0;
vcoef = coef(:);
for k = 1:N1
    [Nz Nx] = size(W(:,:,k));
    t1 = Nz*Nx;
    Wf(:,:,k)=reshape(vcoef(t+1:t+t1),Nz,Nx);
    t = t + t1;
end

x = iswt2(Wf, filter);
end

