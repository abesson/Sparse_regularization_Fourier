function W = odwt2(x, level, wtype)
    [coef, S] = wavedec2(x, level, wtype);
    W= coef;
end
