function im = iodwt2(x, S, wtype)
    im = conj(waverec2(x, S, wtype));
end