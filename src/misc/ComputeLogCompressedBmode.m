function [bmodeLog] = ComputeLogCompressedBmode(bmode,dBRange)

    %---------------------------------------------------------------------
    %-- Parameters checking
    flagdBRange = 0;
    if exist('dBRange','var')
        flagdBRange = 1;
    end

    bmodeLog = 20*log10(bmode);
    if (flagdBRange)
        if (dBRange<0)
            bmodeLog(bmodeLog<dBRange) = dBRange;
        else
            bmodeLog(bmodeLog<-dBRange) = -dBRange;
        end
    end
    
end
