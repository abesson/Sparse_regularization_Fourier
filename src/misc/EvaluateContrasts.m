function [CR1,CR2,info] = EvaluateContrasts(img,param)
    
    %---------------------------------------------------------------------
    %-- Parameters checking
    if ~isfield(param,'x')
        x = 1:size(img,2);
    else
        x = param.x;
    end
    if ~isfield(param,'z')
        z = 1:size(img,1);
    else
        z = param.z;
    end    
    if ~isfield(param,'xc')
        xc = size(img,2)/2;
    else
        xc = param.xc;
    end
    if ~isfield(param,'zc')
        zc = size(img,1)/2;
    else
        zc = param.zc;
    end
    if ~isfield(param,'r')
        r = size(img,1)/4;
    else
        r = param.r;
    end
    if ~isfield(param,'fact1')
        fact1 = 1;
    else
        fact1 = param.fact1;
    end
    if ~isfield(param,'fact2')
        fact2 = 1;
    else
        fact2 = param.fact2;
    end    
    if ~isfield(param,'flagDisplay')
        flagDisplay = 0;
    else
        flagDisplay = param.flagDisplay;
    end  
    if ~isfield(param,'gammaParam')
        gammaParam = 0.3;
    else
        gammaParam = param.gammaParam;
    end     
    
    
    %---------------------------------------------------------------------    
    %-- Compute the inside/outside regions
    bmode = power(img,gammaParam);
    bmode = uint8(255*bmode);
    bmode = double(bmode);
    [X,Z] = meshgrid(x,z);
    r0 = fact1*r;
    r1 = (2-fact1)*r;
    r2 = r1+fact2*r0;    
    foreground = (X-xc).^2 + (Z-zc).^2 < (r0)^2;
    background = xor(((X-xc).^2 + (Z-zc).^2 < (r2)^2),((X-xc).^2 + (Z-zc).^2 < (r1)^2));
    
    %---------------------------------------------------------------------    
    %-- Compute the mean inside and outside values in pixel
    meanIn = mean(bmode(foreground));
    meanOut = mean(bmode(background));
    stdIn = std(bmode(foreground));  
    stdOut = std(bmode(background));   
    
    %---------------------------------------------------------------------    
    %-- Compute CNR value (saved here as CR2)
    CNR = abs(meanOut-meanIn) / sqrt( (stdOut^2+stdIn^2) / 2 ); %-- equation (2) in the above mentioned paper
    CNR = 20*log10(CNR);        %-- in dB
    CNR = round(CNR*100)/100;
    CR2 = CNR;
    
    %---------------------------------------------------------------------    
    %-- Compute CR value
    CR = abs(meanOut-meanIn)/abs((meanOut+meanIn)/2);  %-- equation (1) in M.C. van Wijk 's paper: Performance testing of medical ultrasound equipment: fundamental vs. harmonic mode
    CR = 20*log10(CR);          %-- in dB
    CR = round(CR*10)/10;
    
    %---------------------------------------------------------------------       
    %-- Compute TCR (tissue to Clutter ratio)
    TCR = 20*log10(meanOut/meanIn);
    TCR = round(TCR*10)/10;
    
    %---------------------------------------------------------------------       
    %-- Compute CR from bmode without log compression 
    meanIn_NonLog = mean(img(foreground));
    meanOut_NonLog = mean(img(background));
    CR1 = 20*log10(meanIn_NonLog/meanOut_NonLog);
    CR1 = round(CR1*100)/100;
    
    %---------------------------------------------------------------------    
    %-- Save information structure
    info = struct('foreground',foreground,'background',background,...
        'meanIn',meanIn,'meanOut',meanOut,'stdIn',stdIn,'stdOut',stdOut,...
        'CR',CR,'TCR',TCR,'CNR',CNR,'CR1',CR1,'CR2',CR2);
    
    
    %---------------------------------------------------------------------    
    %-- Display results if required
    if (flagDisplay==1)
        figure; imagesc(x,z,bmode); colormap(gray); axis image;
        hold on; contour(x,z,background,[0 0],'k','linewidth',3);
        hold on; contour(x,z,foreground,[0 0],'r','linewidth',3);
    end    
    
    
end
