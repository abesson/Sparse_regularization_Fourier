function [x,z,bmode,migSIG,param,migSIGRef] = lumig(SIG,param)

%LUMIG   Lu's migration for plane wave imaging
%   MIGSIG = LUMIG(SIG,PARAM) performs a migration of the signals stored in
%   the array SIG using Lu's method. MIGSIG contains the migrated signals.
%   PARAM is a structure that contains all the parameter values required
%   for the migration (see below for more details).
%
%   [MIGSIG,PARAM] = LUMIG(SIG,PARAM) also returns the complete list of
%   parameters. PARAM.z corresponds to the depth values at which the
%   migrated signals are returned.
%
%   Important details on LUMIG:
%   --------------------------
%   1) The signals - typically RF signals - in SIG must be acquired using a
%      PLANE WAVE configuration with a linear array as used in ultrafast
%      ultrasound imaging. If SIG is 2-D, each column corresponds to a
%      single RF signal over time, with the FIRST COLUMN corresponding to
%      the FIRST ELEMENT.
%   2) Several consecutive acquisitions of same size can be stored in a 3-D
%      array for multi-angle compounding. In that case, the migrated output
%      corresponds to the average of the individual migrated signals.
%   3) The steering angle is positive (PARAM.TXangle > 0) if the 1st
%      element is the first to transmit. The steering angle is negative
%      (PARAM.TXangle < 0) if the last element is the first to transmit.
%   4) A bandpass filter can be used to smooth the migrated signals (see
%      PARAM.passband below).
%
%   PARAM is a structure that contains the following fields:
%   -------------------------------------------------------
%   1) PARAM.fs: sample frequency (in Hz, REQUIRED)
%   2) PARAM.pitch: pitch of the linear transducer (in m, REQUIRED)
%   3) PARAM.TXangle: steering (transmit) angles (in rad, default = 0)
%            One must have numel(PARAM.TXangle) = size(SIG,3).
%            PARAM.TXangle can also be a scalar.
%   4) PARAM.c: longitudinal velocity (in m/s, default = 1540 m/s)
%   5) PARAM.t0: acquisition start time (in s, default = 0)
%   6) PARAM.p: p-point sinc interpolation (default: p = 0)
%            A sinc interpolator is used, as described in Harlan, 1982,
%            "Avoiding interpolation artifacts in Stolt migration".
%            If PARAM.p = 0, then a linear interpolation is used (default).
%   7) PARAM.passband: Passband of the bandpass filter (default = [0,1]).
%            Must be a 2-element vector [w1,w2], where 0<w1<w2<1.0, with
%            1.0 corresponding to half the sample rate (i.e. param.fs/2).
%            A 30th order bandpass FIR filter is used.
%            Example: If you measured the RF signals at a sample rate of 20
%            MHz with a linear array whose passband is 3-7 MHz, you may use
%            PARAM.passband = [3e6,7e6]/(20e6/2) = [0.3 0.7].
%
%   Reference
%   ---------
%   Chen and Lu, Extended high-frame rate imaging method with
%   limited-diffraction beams. IEEE TUFFC, 53, p 880-899, 2006.
%   <a
%   href="matlab:web('http://dx.doi.org/10.1109/TUFFC.2006.1632680')">Paper here</a>
%
%   See also FKMIG, RFMIG
%
%   -- Damien Garcia -- 2012/10
%   website: <a
%   href="matlab:web('http://www.biomecardio.com')">www.BiomeCardio.com</a>

[nt,nx,Nframes] = size(SIG);
% dBRange = param.dBRange;

%--- Check input parameters ---
if ~isfield(param,'c')
    param.c = 1540; % longitudinal velocity in m/s
end
if ~isfield(param,'fs')
    error(['A sample frequency (fs) ',...
        'must be specified as a structure field.'])
end
if ~isfield(param,'t0') % in s
    if isfield(param,'z0')
        param.t0 = param.z0/param.c*2; % param.z0 was used in the old version
    else
        param.t0 = zeros(1,Nframes); % acquisition time start in s
    end
end
if isscalar(param.t0)
    param.t0 = param.t0*ones(1,Nframes);
end
assert(numel(param.t0)==Nframes,...
    'PARAM.t0 must be a scalar or a vector of length number_of_frames.')
if ~isfield(param,'pitch') % in m
    if isfield(param,'dx')
        param.pitch = param.dx; % param.dx was used in the old version
    else
        error('A pitch value (PARAM.pitch) is required.')
    end
end
if ~isfield(param,'TXangle')
    param.TXangle = 0;
end
if isscalar(param.TXangle)
    param.TXangle = param.TXangle*ones(1,Nframes);
end
assert(numel(param.TXangle)==Nframes,...
    'PARAM.TXangle must be a scalar or a vector of length number_of_frames.')
if ~isfield(param,'p')
    param.p = 0;
end
assert(param.p>=0 & param.p==round(param.p),...
    ['PARAM.p must be a positive integer (p-point sinc interpolation)',...
    ' or zero (linear interpolation).'])
if isfield(param,'passband')
    wn = param.passband;
else
    wn = [0 1];
end
assert(numel(wn)==2 & wn(1)<wn(2) & wn(1)>=0 & wn(2)<=1 ,...
    'PARAM.passband must be a 2-element vector [w1,w2] with 0<w1<w2<1.')

SIG = double(SIG);

%-- Temporal shift
t0 = param.t0;
ntshift = max(round(t0*param.fs));

%-- Zero-padding before FFTs
if param.p
    % slight 0-padding with sinc interpolation
    ntFFT = round(1.5*nt)+ntshift;
else
    % extensive 0-padding is required with linear interpolation
    ntFFT = 4*nt+ntshift;
end
if rem(ntFFT,2)==1 % ntFFT must be even
    ntFFT = ntFFT+1;
end
nxFFT = round(4*nx); % in order to avoid lateral edge effects
if rem(nxFFT,2)==1 % nxFFT must be even
    nxFFT = nxFFT+1;
end

%-- Create the filter if a passband is given
if ~isequal(wn(:),[0;1])
    hf = freqz(fir1(30,wn),1,ntFFT/2+1);
else
    hf = 1;
end

f0 = (0:ntFFT/2)'*param.fs/ntFFT;
kx = [0:nxFFT/2 -nxFFT/2+1:-1]/param.pitch/nxFFT;
[kx,f] = meshgrid(kx,f0);

migSIG = zeros(ntFFT,nxFFT);
for k = 1:Nframes
    
    %-- Temporal FFT
    SIGk = fft(SIG(:,:,k),ntFFT);
    % The signal is real: only the positive temporal frequencies are kept:
    SIGk(ntFFT/2+2:ntFFT,:,:) = [];
    
    sinA = sin(param.TXangle(k));
    cosA = cos(param.TXangle(k));
    
    %-- Compensate for steering angle and/or depth start
    if sinA~=0 || t0(k)~=0
        dt = sinA*((nx-1)*(param.TXangle(k)<0)-(0:nx-1))*...
            param.pitch/param.c; % steering angle
        tmp = bsxfun(@times,f0,dt+t0(k)); % depth start
        SIGk = SIGk.*exp(-2*1i*pi*tmp);
    end
    
    %-- Spatial FFT
    SIGk = fft(SIGk,nxFFT,2);
  
    % Note: we choose kz = 2*f/c (i.e. z = c*t/2);
    kz = 2*f/param.c;
    fkz = param.c/2*(kx.^2+kz.^2)./(kz*cosA+kx*sinA+eps);
    
    
    isevanescent = abs(kz)./abs(kx)<1;
    fkz(isevanescent) = 0;
    SIGk(isevanescent) = 0;

    %-- Interpolation in the frequency domain: f -> fkz
    if param.p
        %- p-point sinc interpolation
        SIGk = interpSINC(param.fs/ntFFT,SIGk,fkz(:,1:nxFFT/2+1),param.p,([0 1]+wn)/2);
    else
        %- linear interpolation
        SIGk = interpLIN(param.fs/ntFFT,SIGk,fkz,([0 1]+wn)/2);
    end
    % imagesc(abs(SIGk)),pause
    
    %-- Filtering
    if hf~=1, SIGk = bsxfun(@times,SIGk,hf); end
    
    %-- f-IFFT
    SIGk = [SIGk; conj([SIGk(ntFFT/2:-1:2,1) SIGk(ntFFT/2:-1:2,end:-1:2)])]; %#ok
    SIGk = ifft(SIGk);
    
    %-- Averaging
    migSIG = ((k-1)*migSIG + SIGk)/k;
end

%-- Final migrated signal
migSIG = ifft(migSIG,[],2,'symmetric'); % kx-IFFT
if hf~=1 % remove the phase delay due to the filter
    migSIG = migSIG([16:ntFFT 1:15],:);
    migSIG(ntFFT-14:ntFFT,:) = 0;
end
migSIGRef = migSIG;
migSIG = migSIG((1:nt)+ntshift,1:nx);

x = ((0:nx-1)-(nx-1)/2)*param.pitch;
z = (0:nt-1)*param.c/param.fs/2 + max(param.t0*param.c/2);
% %--TGC
%     for i = 1:size(migSIG,2)
%     migSIG(:,i) = migSIG(:,i).*power(10,(0.5*5.*z'));
%     end
%-- Compute the corresponding bmode image without log compression    
hilb = hilbert(migSIG);
env = abs(hilb);
bmode = env/max(max(env));


end

function yi = interpLIN(dx,y,xi,wn)
% -- Linear interpolation along columns
siz = size(y);
yi = zeros(siz);
n1 = round(wn(1)*(siz(1)-1)+1);
n2 = round(wn(2)*(siz(1)-1)+1);

% -- Classical interpolation
idx = xi/dx + 1;
I = idx<2 | idx>(siz(1)-1);

idx(I) = 1; % arbitrary junk index
idxf = floor(idx);
for k = 1:siz(2)
    idxfk = idxf(n1:n2,k);
    idxk = idxfk-idx(n1:n2,k);
    yi(n1:n2,k) = y(idxfk,k).*(idxk+1) - y(idxfk+1,k).*idxk;
end
yi(I) = 0;
end

function yi = interpSINC(dx,y,xi,p,wn)
% -- Sinc interpolation --
% Harlan, 1982, "Avoiding interpolation artifacts in Stolt migration"
%                cf. equation (4) page 109
% --
% A p-point sinc interpolator is used
% --

%-- SINC approximation around 0:
% sinc0(x) ~ sinc(x) for x in [-.5,.5]
% sinc0(1) = 1 and sinc0(0.5) = sinc(0.5)
sinc0 = @(x) 1+(8/pi-4)*x.^2;

siz = size(y);
yi = zeros(siz);
n1 = round(wn(1)*(siz(1)-1)+1);
n2 = round(wn(2)*(siz(1)-1)+1);

xi = xi/dx + 1; % n + delta_n
idxr = round(xi); % n
delta_n = xi-idxr;
SINmat = sin(pi*delta_n);
EXPmat = cos(pi*delta_n)-1i*SINmat;

mp = [floor(-p/2+1):-1 1:floor(p/2)];
for k = 2:siz(2)/2+1
    yk1 = y(:,k);
    yk2 = y(:,end-k+2);
    idxrk = idxr(n1:n2,k);
    delta_nk = delta_n(n1:n2,k);
    for m = mp
        C = 1./(delta_nk-m);
        % C = (1-abs(delta_nk-m)/floor(p/2+1))./(delta_nk-m); % Triangle tapered
        % C = 1./tan(pi*(delta_nk-m)/p); % Muir's function tapered (see Fomel 2001)
        idxk = idxrk+m;
        
        % idxk = mod(idxk-1,siz(1))+1;
        I = idxk<1 |idxk>siz(1);
        C(I) = 0; idxk(I) = 1; % arbitrary junk index (since C(I)=0)
        
        yi(n1:n2,k) = yi(n1:n2,k) + C.*yk1(idxk);
        if k<siz(2)/2+1
            yi(n1:n2,end-k+2) = yi(n1:n2,end-k+2) + C.*yk2(idxk);
        end
    end
    yi(n1:n2,k) = 1/pi*SINmat(n1:n2,k).*yi(n1:n2,k);
    if k<siz(2)/2+1
        yi(n1:n2,end-k+2) = 1/pi*SINmat(n1:n2,k).*yi(n1:n2,end-k+2);
    end
    
    % idem for m = 0
    % ---
    % idxrk = mod(idxrk-1,siz(1))+1;
    I = idxrk>siz(1);
    idxrk(I) = 1; % arbitrary junk index
    delta_nk(I) = (4-8/pi)^-.5; % so that sinc0(delta_nk(I)) = 0
    
    S = sinc0(delta_nk);
    yi(n1:n2,k) = yi(n1:n2,k) + yk1(idxrk).*S;
    if k<siz(2)/2+1
        yi(n1:n2,end-k+2) = yi(n1:n2,end-k+2) + yk2(idxrk).*S;
    end
    
    yi(n1:n2,k) = yi(n1:n2,k).*EXPmat(n1:n2,k);
    if k<siz(2)/2+1
        yi(n1:n2,end-k+2) = yi(n1:n2,end-k+2).*EXPmat(n1:n2,k);
    end
    
end
end

