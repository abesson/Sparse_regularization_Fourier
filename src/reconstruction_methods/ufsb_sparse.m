function [bmode, sol, param] = ufsb_sparse(SIG, param, transform)
%ufsb_sparse - Reconstruct ultrasound image from sparse regularization approach with ufsb
%
% Syntax:  [bmode,migSIG,param] = ufsb_sparse(SIG,param,transform)
%
% Inputs:
%    SIG - element raw data (stored as a 3D matrix (number of lines * number of transducers * number of frames))
%    param - structure containing various parameter related to the probe, medium (cf. lumig.m for more details)
%    transform - structure which contains the parameter related to the sparsifying transform:
%			- transform.wtransform: type of the sparsifying trasnform. It could be 'sara', 'orthogonalwavelet', 'undecimatedwavelet' or 'dirac'
%			- transform.motherfunction: Mother function for the wavelet-based models
%			- transform.level: level of the decomposition for the wavelet-based models
%			- transform.beta: sparsity promoting parameter (between 0 and 1)
% Outputs:
%    bmode - bmode image (normalized envelope)
%    migSIG - RF image
%	 param - structure which contains details related to the image, medium and probe
%
%
% Other m-files required: Fessler's irt toolbox needed (for the non-uniform Fourier transform) 
% Subfunctions: none
%
% See also: UltrasoundFourierSliceBeamformingLinear

% Author: Adrien Besson
% Signal processing laboratory (LTS5), Ecole polytechnique Federale de Lausanne
% email address: adrien.besson@epfl.ch  
% August 2016

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              UFSB with sparse-based reconstruction                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-- Control of the input variables
[nz,nx, Nframes] = size(SIG);
%----- Input parameters -----
%-- 1) Speed of sound
if ~isfield(param,'c')
    param.c = 1540; % longitudinal velocity in m/s
end
c = param.c;
%-- 2) Sample frequency
if ~isfield(param,'fs')
    error(['A sample frequency (fs) ',...
        'must be specified as a structure field.'])
end
%-- 3) Acquisition start time
if ~isfield(param,'t0') % in s
    param.t0 = zeros(1,Nframes); % acquisition time start in s
end
if isscalar(param.t0)
    param.t0 = param.t0*ones(1,Nframes);
end
assert(numel(param.t0)==Nframes,...
    'PARAM.t0 must be a scalar or a vector of length number_of_frames.')
%-- 4) Pitch
if ~isfield(param,'pitch') % in m
    error('A pitch value (PARAM.pitch) is required.')
end
%-- 5) Transmit angle
if ~isfield(param,'TXangle')
    param.TXangle = 0; % in rad
end
if isscalar(param.TXangle)
    param.TXangle = param.TXangle*ones(1,Nframes);
end
assert(numel(param.TXangle)==Nframes,...
    'PARAM.TXangle must be a scalar or a vector of length number_of_frames.')
%-- 6) Settings for UFSB
if ~isfield(param,'settings') % in m
    param.settings = struct('flagfftRF',1, 'dxi', deg2rad(0.5), 'ximax', deg2rad(50));
end
%-- 7) Transform
if (numel(fieldnames(transform)) < 4)
    if (~isfield(transform,'wtranform'))
        error('Please specify the field transform.wtransform with: dirac, orthogonalwavelet, undecimatedwavelet, sara');
    end
    if(~isfield(transform,'level'))
        %-- Case of a Dirac basis, no need for level
        if (~strcmp(lower(transform.wtransform), 'dirac'))
            disp('No decomposition level (trasnform.level) specified for wavelet -> default value set to 1');
            transform.level = 1;
        end
    end
    if(~isfield(transform,'wfilter'))
        %-- Case of a Dirac basis, no need for filter
        if (~strcmp(lower(transform.wtransform), 'dirac'))
            disp('No mother function (transform.wfilter) specified for wavelet -> default value set to db4');
            transform.wfilter = 'db4';
        end
    end
    if(~isfield(transform,'beta'))
        disp('No sparsity promoting parameter (transform.beta) specified -> default value set to 0.5');
        transform.beta = 0.5;
    else
        if (transform.beta <0 || transform.beta > 1)
            error('Wrong value for the  sparsity promoting parameter: it must be between 0 and 1');
        end
    end
end
%----- end of Input parameters -----
%-- Fix acquisition Fourier space limits
flag_ximax = 0;
flag_dxi = 0;
settings = param.settings;
if exist('settings','var')
    if isfield(settings,'ximax')
        ximax = settings.ximax;
        disp(['ximax = ',num2str(rad2deg(ximax))])
        flag_ximax = 1;
    end
    if isfield(settings,'dxi')
        dxi = settings.dxi;
        disp(['dxi = ',num2str(rad2deg(dxi))])
        flag_dxi = 1;
    end
end

%-- Fix acquisition Fourier space limits
if (flag_ximax==0)
    B = 2*param.bandwidth(1)/param.c0;
    ximax = 2 * atan(1/2/param.Pitch/B);
    disp(['ximax = ',num2str(rad2deg(ximax))])
end

%-- Parameter check
c = param.c0;
angles = param.TXangle;

%-- Fix Object Fourier Space limits
%-- Compute dx and then nx
dx = param.Pitch;
nxFFT = 1*nx;         %-- zero padding in x to push away the side artefact
if rem(nxFFT,2)==1    %-- nxFFT must be even
    nxFFT = nxFFT+1;
end

%-- Compute dz and then nz
dz = c/(2*param.fs);
nzFFT = 2*nz;         %-- zero padding in z to push away the side artefact
if rem(nzFFT,2)==1    %-- nzFFT must be even
    nzFFT = nzFFT+1;
end

%-- Grid coordinates
x = ((0:nx-1)-(nx-1)/2)*dx;
z = ((0:nz-1))*dz;

%-- Define K-space and frequency sampling
kx = [0:nxFFT/2 -nxFFT/2+1:-1]/dx/nxFFT;   %-- The way Damien codes his functions
kz = [0:nzFFT/2 -nzFFT/2+1:-1]/dz/nzFFT;
freq = linspace(0,param.fs/2,nzFFT/2+1);

%-- Fix acquisition Fourier space dxi
if (flag_dxi==0)
    fov = 2*ximax/2;
    k1 = param.bandwidth(1)*2/param.c0;
    k2 = param.bandwidth(2)*2/param.c0;
    Nr = length(find(kz>k1 & kz<k2));
    dkx = kx(2)-kx(1);
    dkz = kz(2)-kz(1);
    dxi = 2 * fov / ( Nr / (dkx/dkz) );
    disp(['dxi = ',num2str(rad2deg(dxi))])
end
nxiFull = ceil(2*ximax/dxi);

%-- Loop for each steered plane wave
G = []; y = [];

disp('********** Creation of the measurement matrix **********')
for its=1:Nframes
    %-- Get current rawdata and angle
    rawdata = SIG(:,:,its);
    dt = param.t0;
    if (length(angles)==1)
        angle = angles;
    else
        angle = angles(its);
    end
    
    %-- Update the xi range according to the emitted angle value
    xi = linspace(-ximax+angle,ximax+angle,nxiFull);
    nxi = length(xi);
    
    %-- Compute the Fourier projections
    S = zeros(nzFFT/2+1,nxi);
    RF = fft(rawdata,nzFFT);
    
    %-- The signal is real: only the positive temporal frequencies are kept
    RF = RF(1:nzFFT/2+1,:);
    for u=1:numel(xi)
        tau = (param.xm * sin(xi(u)) / c) - dt(its);
        delay = exp(-2i*pi*freq'*tau);
        S(:,u) = sum(RF.*delay,2);
    end
    
    %-- Fourier data for reconstruction
    [Nv,~] = size(S);
    % Non-uniform K-space
    k = freq/c;
    Kx1=k'*(sin(angle) + sin(xi));
    Kz1=k'*(cos(angle)+ cos(xi));
    Kx1=Kx1*2*pi*dx;
    Kz1=Kz1*2*pi*dz;
    Nv1 = ceil(Nv/2);
    Kx1 =  Kx1(1:Nv1,:);
    Kz1 =  Kz1(1:Nv1,:);
    
    % Fourier samples onto the K-space
    S =  S(1:Nv1,:);
    Kx = Kx1(:);
    Kz = Kz1(:);
    Nx = nx;
    Nz = nz;
    
    %Set measurement operators and data for sparse reconstruction
    y = [y; S(:)];
    G = [G ; Gnufft({[Kz Kx],[Nz Nx],[8 8],[2*Nz 2*Nx], [0, Nx/2]})];
    
end
% Measurement operators for the CS-based algorithm
A = @(x) G*reshape(x, Nz*Nx, 1);
At = @(x) reshape(G'*x, Nz, Nx);

%Calculation of the norm of AtA (used in the algorithm)
disp('********** Calculation of the norm of the measurement matrix (power method) **********')
eval = pow_method(A, At, [Nz,Nx], 1e-3, 100, 0);

% Measurement operators for the CS-based algorithm
A = @(x) A(x) / sqrt(eval);
At = @(x) At(x) / sqrt(eval);

%Sparsifying frames for the CS reconstruction
sol1 = At(y);
switch lower(transform.wtransform)
    case 'sara'
        dwtmode('per');
        [C,S]=wavedec2(sol1,transform.level,'db8');
        ncoef=length(C);
        [C1,S1]=wavedec2(sol1,transform.level,'db1');
        ncoef1=length(C1);
        [C2,S2]=wavedec2(sol1,transform.level,'db2');
        ncoef2=length(C2);
        [C3,S3]=wavedec2(sol1,transform.level,'db3');
        ncoef3=length(C3);
        [C4,S4]=wavedec2(sol1,transform.level,'db4');
        ncoef4=length(C4);
        [C5,S5]=wavedec2(sol1,transform.level,'db5');
        ncoef5=length(C5);
        [C6,S6]=wavedec2(sol1,transform.level,'db6');
        ncoef6=length(C6);
        [C7,S7]=wavedec2(sol1,transform.level,'db7');
        ncoef7=length(C7);
        
        %Sparsity averaging operator
        Psit = @(x) [wavedec2(x,transform.level,'db1')'; wavedec2(x,transform.level,'db2')';wavedec2(x,transform.level,'db3')';...
            wavedec2(x,transform.level,'db4')'; wavedec2(x,transform.level,'db5')'; wavedec2(x,transform.level,'db6')';...
            wavedec2(x,transform.level,'db7')';wavedec2(x,transform.level,'db8')']/sqrt(8);
        
        Psi = @(x) (waverec2(x(1:ncoef1),S1,'db1')+waverec2(x(ncoef1+1:ncoef1+ncoef2),S2,'db2')+...
            waverec2(x(ncoef1+ncoef2+1:ncoef1+ncoef2+ncoef3),S3,'db3')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+1:ncoef1+ncoef2+ncoef3+ncoef4),S4,'db4')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5),S5,'db5')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6),S6,'db6')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7),S7,'db7')+...
            waverec2(x(ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7+1:ncoef1+ncoef2+ncoef3+ncoef4+ncoef5+ncoef6+ncoef7+ncoef),S,'db8'))/sqrt(8);
        
    case 'orthogonalwavelet'
        [~,S]=wavedec2(sol1,transform.level,transform.wfilter);
        Psit = @(x) odwt2(x, transform.level, transform.wfilter);
        Psi = @(x) iodwt2(x, S, transform.wfilter);
        
    case 'undecimatedwavelet'
        Wc = swt2(sol1,transform.level,transform.wfilter);
        Psit = @(x) udwt2(x, transform.level,transform.wfilter);
        Psi = @(x) iudwt2(x, Wc, transform.wfilter);
    case 'dirac'
        Psit = @(x) x;
        Psi = @(x) x;
    otherwise
        error('Wrong sparsifying model: please specify transform.wtransform with sara, orthogonalwavelet, undecimatedwavelet or dirac');
end

% Parameters for admm solver
ss = Psit(sol1);
lambda = 1e-2*norm(abs(ss(:)),inf);
epsilon = transform.beta*norm(y(:));
param1.verbose = 1; % Print log or not
param1.rel_obj = 1e-4; % Stopping criterion for the L1 problem
param1.gamma = lambda;
if (isfield(param, 'max_iter'))
    param1.max_iter = param.max_iter;
else
    param1.max_iter = 200; % Max. number of iterations for the L1 problem
end
% param1.nu = eval; % Bound on the norm of the operator A
param1.tight_L1 = 1; % Indicate if Psit is a tight frame (1) or not (0)
param1.max_iter_L1 = 10;
param1.rel_obj_L1 = 1e-2;
param1.pos_L1 = 0;
param1.nu_L1 = 1;
param1.verbose_L1 = 0;
param1.initsol = sol1;

%-- Sparse Regularization using ADMM
disp('*******  Sparse Regularization *******')
if (param.max_iter > 0)
	[migSIG,~] = admm_bpcon(y, epsilon, A, At, Psi, Psit, param1);
else
	migSIG = sol1;
end


%-- Compute the corresponding bmode image
env = abs(hilbert(migSIG));
bmode = env/max(max(env));
param.x = x;
param.z = z;

end



